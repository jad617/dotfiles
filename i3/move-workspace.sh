#! /bin/bash

get_ws() {
    screen_number=$(i3-msg -t get_workspaces | jq -c '.[] | select(.visible==true).name' | grep -v 11 | wc -l)
    current_ws=($(i3-msg -t get_workspaces | jq '.[] | select(.visible==true).name' |sed 's/"//g' | sort | grep -v 11))
    active_ws=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' |sed 's/"//g')
}


validate_down() {
    get_ws
    if [ $screen_number -eq 1 ]
    then
        if [ "${current_ws[0]}" -ge 11 ]
        then
            exit
        fi
    elif [ $screen_number -gt 1 ]
    then
        if [ "${current_ws[0]}" -ge 9 ]
        then
            exit
        fi
    fi
}

validate_up() {
    get_ws
    if [ "${current_ws[0]}" -le ${screen_number} ]
    then
        exit
    fi
}

keep_active_ws() {
    #Function to get the active ws

    focus=$(($active_ws%2))

    if [ $focus == 1 ]
    then
        echo "left"
        i3-msg focus left
    elif [ $focus == 0 ]
    then
        echo "right"
        i3-msg focus right
    fi
}

move_down() {
    validate_down
    i=0
    while [ $i -lt ${#current_ws[@]} ]
    do
        ((current_ws[$i]+=${screen_number}))
        i3-msg workspace ${current_ws[$i]}
        ((i++))
    done
    keep_active_ws
}

move_up() {
    validate_up
    i=0
    while [ $i -lt ${#current_ws[@]} ]
    do
        ((current_ws[$i]-=${screen_number}))
        i3-msg workspace ${current_ws[$i]}
        ((i++))
    done
    keep_active_ws
}

if [ "$1" == "up" ]
then
    move_up
elif [ "$1" == "down" ]
then
    move_down
fi
