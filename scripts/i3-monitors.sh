#! /bin/bash

#/etc/profile.d/i3-monitors.sh

get_monitors(){
    find_primary=($(xrandr | egrep "\bconnected\b" | awk '{print $1}' | cut -d"-" -f1-2 | uniq ))
    primary_mon=$(xrandr | egrep -i "primary" | awk '{print $1}')
    #monitors=($(xrandr | egrep -i "DP|HDMI" | egrep -i "\bconnected\b" | awk '{print $1}'))
    monitors=($(xrandr | grep "\bconnected\b" | grep -v primary | awk {'print $1'}))
}

setup_monitors(){
get_monitors

killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

 
if [ "${find_primary[0]}" == "DP-1" ] || [ "${find_primary[1]}" == "DP-1" ]
then
    xrandr --output DP-1-1 --primary --output DP-1-2 --right-of DP-1-1 --output eDP-1 --off
    if [ -e ~/.config/i3/config ]
    then
        rm ~/.config/i3/config
    fi
    ln -s ~/workstation/dotfiles/i3/multi_monitors ~/.config/i3/config
    export MONITOR_A="DP-1-1"
    export MONITOR0="DP-1-2"
    polybar bar-primary  &
    polybar bar-0 &
else
    xrandr --output eDP-1 --primary --output HDMI-1 --auto
    if [ -e ~/.config/i3/config ]
    then
        rm ~/.config/i3/config
    fi
    ln -s ~/workstation/dotfiles/i3/default-monitor ~/.config/i3/config
    export MONITOR_A="eDP-1"
    export MONITOR0="HDMI-1"
    polybar bar-primary  &
    polybar bar-0 &
fi
}
