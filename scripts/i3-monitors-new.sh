#! /bin/bash

get_monitors(){
    monitor_list=($(xrandr --listmonitors | awk '{print $4}' | grep [a-Z0-9] | sort))
}

group_monitors(){
    get_monitors
    #We want DP to always go before eDP
    #But eDP should always go before HDMI
    DP="DP-1"
    eDP="eDP-1"
    HDMI="HDMI"


    a=0
    x=0
    y=0
    z=0
    while [ $a -lt ${#monitor_list[@]} ]
    do 
        if [[ "${monitor_list[$a]}" == "$DP"* ]]
        then
            DP_result=${monitor_list[$a]}
            DP_array[$x]="$DP_result"
            ((x++))
        elif [[ "${monitor_list[$a]}" =~ $eDP ]]
        then
            eDP_result=${monitor_list[$a]}
            eDP_array[$y]="$eDP_result"
            ((y++))
        elif [[ "${monitor_list[$a]}" =~ $HDMI ]]
        then
            HDMI_result=${monitor_list[$a]}
            HDMI_array[$z]="$HDMI_result"
            ((z++))
        fi
        ((a++))
    done
    

}

group_DP(){

    if [ ${#eDP_array[@]} -gt 0 ]
    then
        eDP_exist=true
    fi
    if [ ${#DP_array[@]} -eq 0 ] && [ $eDP_exist ]
    then
        xrand_output="xrandr --output ${DP_array[0]} --primary  --output eDP-1 --right-of ${DP_array[0]}"
    fi

    if [ ${#DP_array[@]} -gt 1 ]
    then
        primary=${DP_array[0]}

        b=1
        while [ $b -lt ${#DP_array[@]} ]
        do
            #xrandr --output DP-1-1 --primary --output DP-1-2 --right-of DP-1-1 --output eDP-1 --off
            DP_output="--ouput ${DP_array[$b]} --right-of ${DP_array[$b-1]} "
            xrand_array[$b]="$DP_output"
            ((b++))
        done
        
        if [ $eDP_exist ]
        then
            xrand_output="xrandr --output $primary --primary ${xrand_array[@]} --output eDP-1 --off"
         else
            xrand_output="xrandr --output $primary --primary ${xrand_array[@]}"
         fi
    fi
}

group_HDMI(){

    if [ ${#HDMI_array[@]} -gt 1 ]
    then
        d=1
        while [ $d -lt ${#HDMI_array[@]} ]
        do
        #xrandr --output DP-1-1 --primary --output DP-1-2 --right-of DP-1-1 --output eDP-1 --off
        HDMI_output=("--ouput ${HDMI_array[$d]} --right-of ${HDMI_array[$d-1]} ")
        ((d++))
        xrand_output="xrandr --output $primary --primary $HDMI_output"
        done
    elif [ ${#HDMI_array[@]} -eq 1 ]
    then
        xrand_output="xrandr --output $primary --primary --output ${HDMI_array[0]} --right-of $primary"
    fi
}

group_eDP(){

    if [ ${#eDP_array[@]} -gt 0 ] && [ ${#DP_array[@]} -eq 0 ]
    then
        if [ ${#HDMI_array[@]} -gt 0 ]
        then
            HDMI_exist=true
        fi

        primary=${eDP_array[0]}
        if [ ${#eDP_array[@]} -gt 1 ]
        then
            c=1
            while [ $c -lt ${#eDP_array[@]} ] && [ ! $HDMI_exist ]
            do
                #xrandr --output DP-1-1 --primary --output DP-1-2 --right-of DP-1-1 --output eDP-1 --off
                eDP_output="--ouput ${eDP_array[$c]} --right-of ${eDP_array[$c-1]} "
                xrand_array[$c]="$eDP_output"
                ((c++))
            done
            xrand_output="xrandr --output $primary --primary ${xrand_array[@]}"

        elif [ ${#eDP_array[@]} -eq 1 ] && [ $HDMI_exist ]
        then
            group_HDMI
        elif [ ${#eDP_array[@]} -eq 1 ] && [ ! $HDMI_exist ]
        then
            xrand_output="xrandr --output $primary --primary"
        else
            echo "Nothing matches the eDP config" 
        fi
    fi
}




prioritize_monitors(){
group_monitors
group_DP
group_eDP
group_HDMI

}

prioritize_monitors

echo "List of all monitors --> ${monitor_list[@]}"
echo "List of all DP monitors ---> ${DP_array[@]}"
echo "List of all xrand monitors ---> $xrand_output"
echo "List of all eDP monitors ---> ${eDP_array[@]}"
echo "List of all HDMI Monitors ---> ${HDMI_array[@]}"
